def is_triangle(a, b, c):
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''
    return (a < (b + c)) and (b < (a + c)) and (c < (a + b))
